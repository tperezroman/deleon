/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Ezequiel
 */
import lombok.Data;

@Data
public class User {
    private int id;
    private String email;
    private String pass;   
}