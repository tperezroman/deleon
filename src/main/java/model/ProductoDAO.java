/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.Producto;
import java.util.List;
import java.util.Map;
import org.sql2o.Connection;
import util.Sql2oDAO;

/**
 *
 * @author Ezequiel
 */
public class ProductoDAO {
    public Producto buscarProducto(int id_producto) {
        String insertSQL = "SELECT * FROM producto WHERE :id_producto=id";
        try (Connection con = Sql2oDAO.getSql2o().open()) {
             List<Producto> res = con.createQuery(insertSQL)
                    .addParameter("id_producto", id_producto)
                    .executeAndFetch(Producto.class);
             return res.get(0);
        }
    } 
    
    public List<Producto> selectAll() {
        String selectALLSQL = "SELECT * FROM producto;";
        List<Producto> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(selectALLSQL).executeAndFetch(Producto.class);
           return res;
        }    
    } 
    
    
    //prueba seleccionar productos en oferta
    public List<Map<String,Object>> selectProdxProm(){
            String sql = "SELECT * FROM producto,promocion;";
            try (Connection con = Sql2oDAO.getSql2o().open()) {
                    List<Map<String,Object>> res = con
                    .createQuery(sql)
                    .executeAndFetchTable()
                    .asList();
                return res;
            }
    }
}
