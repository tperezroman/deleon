/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.User;
import java.util.List;
import org.sql2o.Connection;
import util.Sql2oDAO;
/**
 *
 * @author Ezequiel
 */
public class UserDAO {

    public List<User> selectUsuario( String email, String pass) {
        
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            String sql = "SELECT * FROM user WHERE email = :email and  pass = :pass";

            List<User> usuarios = con
                .createQuery(sql)
                .addParameter("email", email)
                .addParameter("pass", pass)
                .executeAndFetch(User.class);
            return usuarios;
        }   
    }
    
    public User selectxEmail( String email) {
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            String sql = "SELECT * FROM user WHERE email = :email";

            List<User> usuarios = con
                .createQuery(sql)
                .addParameter("email", email)
                .executeAndFetch(User.class);
            return usuarios.get(0);
        }   
    }
}
