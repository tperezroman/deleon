/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.sql2o.Connection;
import util.Sql2oDAO;

/**
 *
 * @author Ezequiel
 */
public class CalificacionDAO {
    public void insertCalificacion(int id_usuario,int id_producto, int calificacion, String comentario) {
        String insertSQL =  "insert into calificacion (id_usuario,id_producto,puntuacion,comentario)" + "values (:id_usuario,:id_producto,:calificacion,:comentario)";
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con
            .createQuery(insertSQL)
            .addParameter("id_usuario",id_usuario)
            .addParameter("id_producto",id_producto)
            .addParameter("calificacion",calificacion)
            .addParameter("comentario",comentario)
            .executeUpdate();
            
        } catch(Exception e){
            System.out.println("Error al agregar calificacion");
        }
    }
}
