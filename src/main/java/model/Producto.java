/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import lombok.Data;
/**
 *
 * @author Ezequiel
 */

@Data
public class Producto {
    private int id;
    private String nombre;
    //private String descripcion;
    private Double precio;
   //private String especificacion;
    //private String marca;
    //private Date fecha_venc; 
    //private int stock;
    private String imagen;
}
