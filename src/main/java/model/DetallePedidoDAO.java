/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import java.util.Map;
import org.sql2o.Connection;
import util.Sql2oDAO;

/**
 *
 * @author Martin
 */
public class DetallePedidoDAO {
    /*  COMO INSERTAR  UN NUEVO ELEMENTO EN UNA TABLA DONDE LA CLAVE PRIMARIA TIENE AUTO INCREMENT
    
        Sometimes you need to insert data into a database table with an
        autoincremented id (Called identity column in some databases).
        For this to work, you have to tell sql2o to fetch keys after insert.
        This is done with an overload of the createQuery method:
        createQuery(String sql, boolean returnGeneratedKeys)
        To fetch the actual inserted value, call the getKey() method after executing the statement.
    */
    
    public void agregarProducto(int id_producto,int id_pedido) {
        System.out.println("ID del producto que se intenta insertar: " + id_producto);
        int cantidad = 1;
        //por ahora lo dejo estatico, tenemos que consultarlo previamente con la BD
        String insertSQL =  "insert into detalle_pedido (id_pedido,id_producto,cantidad)" + "values (:id_pedido,:id_producto,:cantidad)";
        System.out.println(insertSQL);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con
            .createQuery(insertSQL)
            .addParameter("id_pedido",id_pedido)
            .addParameter("id_producto",id_producto)
            .addParameter("cantidad",cantidad)
            .executeUpdate();
            
        } catch(Exception e){
            System.out.println("Error al agregar producto al carrito");
        }
    }
    
    
    public void vaciarCarro() {
//        int id_carrito = 1;

        String deleteSQL =  "TRUNCATE TABLE detalle_pedido";
        System.out.println(deleteSQL);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con
            .createQuery(deleteSQL)
//            .addParameter("id_carrito",id_carrito)
            .executeUpdate();
            
        } catch(Exception e){
            System.out.println("Error al vaciar el detalle del carrito");
        }
    }
    
    public List<Producto> selectProdDelCarro() {
        String sql = "SELECT producto.* "
                + "FROM producto,detalle_pedido, pedido WHERE "
                + "producto.id=detalle_pedido.id_producto "
                + "and pedido.id_pedido=detalle_pedido.id_pedido "
                + "and pedido.estado like '%en proceso%';";
        List<Producto> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(sql).executeAndFetch(Producto.class);
           System.out.println("Respuesta a la consulta sql: " + res);
           return res;
        }    
    }

    public List<Map<String,Object>> selectComplejo() {
        String sql = "SELECT * FROM producto,detalle_pedido,pedido WHERE "
                + "producto.id=detalle_pedido.id_producto "
                + "and pedido.id_pedido=detalle_pedido.id_pedido "
                + "and pedido.estado like '%en proceso%';";
        List<Map<String,Object>> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(sql).executeAndFetchTable().asList();
           System.out.println("Respuesta al SELECT COMPLEJO: " + res);
           return res;
        }    
    }
    
    //funciona, no lo estamos usando igual. Dejamos el selectxID.
    //lo dejamos aca a modo de ejemplo y por si mas adelante nos sirve
       public int selectCantxID(int id_producto){
       String sql = "SELECT cantidad FROM detalle_pedido WHERE id_producto like :id_producto;";
            try (Connection con = Sql2oDAO.getSql2o().open()) {
                int res = con
                    .createQuery(sql)
                    .addParameter("id_producto", id_producto)
                    .executeScalar(Integer.class);
                System.out.println("Cantidad dentro del dao: " + res);
                return res;
            }
    }

    public void update(int id_producto, int nueva_cantidad, int id_pedido) {
        String updateSQL = "UPDATE detalle_pedido SET  cantidad=:nueva_cantidad WHERE id_producto = :id_producto and id_pedido=:id_pedido;";
        try (Connection con = Sql2oDAO.getSql2o().open()) {
          con.createQuery(updateSQL)
                .addParameter("id_pedido", id_pedido)
                .addParameter("id_producto", id_producto)
                .addParameter("nueva_cantidad", nueva_cantidad)
                .executeUpdate();
        }catch(Exception e){System.out.println("Error al actualizar");}
    }
    
    //Retorna una instancia de detalleCarrito con el id
    public List<DetallePedido> selectxID(int id_producto){
       String sql = "SELECT detalle_pedido.* FROM detalle_pedido,pedido WHERE "
               + "detalle_pedido.id_pedido=pedido.id_pedido "
               + "and detalle_pedido.id_producto=:id_producto "
               + "and pedido.estado LIKE 'en proceso';";
            try (Connection con = Sql2oDAO.getSql2o().open()) {
                List<DetallePedido> res = con
                    .createQuery(sql)
                    .addParameter("id_producto", id_producto)
                    .executeAndFetch(DetallePedido.class);
                System.out.println("List<DetalleCarrito>: " + res);
                return res;
            }
    }
}
