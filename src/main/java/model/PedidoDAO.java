/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import java.util.Map;
import org.sql2o.Connection;
import util.Sql2oDAO;
/**
 *
 * @author martin
 */
public class PedidoDAO {
    public List<Map<String,Object>> selectPedidos() {
        String sql = "SELECT * FROM pedido,detalle_pedido,producto WHERE "
                + "pedido.id_pedido = detalle_pedido.id_pedido "
                + "and detalle_pedido.id_producto = producto.id  "
                + "and pedido.estado LIKE '%pagado%' ;";
        List<Map<String,Object>> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(sql).executeAndFetchTable().asList();
           System.out.println("Respuesta al SELECT PEDIDOS: " + res);
           return res;
        }
        
    }       
        
    public void PedidoEnProceso(int id_usuario){
//        int total = 19999;
        String estado ="en proceso";

        String insertSQL =  "insert into pedido (id_usuario,estado)" + "values (:id_usuario,:estado);";
        System.out.println(insertSQL);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con
            .createQuery(insertSQL)
            .addParameter("id_usuario",id_usuario)
            .addParameter("estado",estado)
            .executeUpdate();
            
        } catch(Exception e){
            System.out.println("Error al agregar pedido");
        }
    }
    
    public Pedido getPedidoEnProceso(){
        String estado ="en proceso";
        
        String sql =  "select * from pedido where estado like :estado;";
        List<Pedido> pedido=null;
        System.out.println(sql);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            pedido = con.createQuery(sql)
                    .addParameter("estado", estado)
                    .executeAndFetch(Pedido.class);
            
        } catch(Exception e){
            System.out.println("Error al agregar pedido");
        }
        return pedido.get(0);
    }
    
    
        public void actualizarPedido(int total){
        String estado ="en proceso";
        String newEstado="pagado";
        String insertSQL =  "UPDATE pedido SET  estado=:newEstado, total=:total WHERE estado LIKE :estado;";
        System.out.println(insertSQL);
        try (Connection con = Sql2oDAO.getSql2o().open()) {
            con
            .createQuery(insertSQL)
            .addParameter("estado",estado)
            .addParameter("newEstado",newEstado)
            .addParameter("total",total)
            .executeUpdate();
            
        } catch(Exception e){
            System.out.println("Error al actualizar pedido");
        }
    }
        
        
}
