/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;
import org.sql2o.Connection;
import model.Producto;
import util.Sql2oDAO;

/**
 *
 * @author Martin
 */
public class ProdxpromDAO {
    
    public List<Producto> selectProdConOfertas() {
        String sql = "SELECT producto.id,producto.nombre,producto.precio,producto.imagen FROM producto,prodxprom WHERE producto.id=prodxprom.id_prod;";
        List<Producto> res = null;
        try (Connection con = Sql2oDAO.getSql2o().open()) {
           res = con.createQuery(sql).executeAndFetch(Producto.class);
           return res;
        }    
    } 
}
