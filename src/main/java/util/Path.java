/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import lombok.Getter;

/**
 *
 * @author Ezequiel
 */
public class Path {
    // Los metodos @Getter son necesarios para acceder desde las variables de Templates de Velocity
    // NO USAR ACCESOS DIRECTOS, SINO SIEMPRE A TRAVÉS DE ESTA CLASE
    public static class Web {
        @Getter public static final String LOGIN = "/api/getLogin/";
        @Getter public static final String LOGOUT = "/api/logout/";
        
        
        @Getter public static final String INDEX = "/api/index/";//para probar
        

       
        @Getter public static final String GET_OFERTAS = "/api/getOfertas/";
        
        @Getter public static final String CALIFICAR = "/api/calificar/";
        @Getter public static final String GUARDARCALI = "/api/guardarCali/";
        //agregar al carro
        @Getter public static final String AGREGARALCARRITO = "/api/agregarAlCarrito/";
        @Getter public static final String MOSTRARCARRITO = "/api/getCarrito/";
        @Getter public static final String MOSTRARDETALLECARRITO = "/api/getDetalleCarrito/";
        @Getter public static final String LISTARCOMPRAS = "/api/getCompras/";
        
        @Getter public static final String AGREGARPEDIDO = "/api/agregarPedido/";
        //MOBILE
        @Getter public static final String INDEX_MOBILE = "/api/mindex/";//para probar
        @Getter public static final String LISTARCOMPRAS_MOBILE = "/api/mgetCompras/";
    }
    
    public static class Template {
        public final static String LAYOUT = "templates/layout.vtl";
        public final static String LAYOUT_LOGIN = "templates/layout_login.vtl";
        
        //public final static String INDEX = "templates/productos.vtl";
        //
        public final static String INDEX = "templates/index.vtl";
        //
        public final static String LOGIN = "templates/login.vtl";
        public final static String CALIFICAR = "templates/calificar.vtl";
       public final static String MOSTRAR_OFERTAS = "templates/ofertas.vtl";
       public final static String MOSTRAR_CARRITO = "templates/carrito.vtl";
       public final static String MOSTRAR_DETALLE_CARRITO = "templates/detalle_carrito.vtl";
       public final static String LISTAR_COMPRAS = "templates/listar_compras.vtl";
       //mobile
       public final static String LISTAR_COMPRAS_MOBILE = "templates/listar_compras_mobile.vtl";
       public final static String LAYOUT_MOBILE = "templates/layout_mobile.vtl";
       public final static String INDEX_MOBILE = "templates/index_mobile.vtl";
    }

}
