/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ProdxpromDAO;
import Principal.Menu;
import java.util.HashMap;
import java.util.List;
import model.ProductoDAO;
import model.Producto;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

/**
 *
 * @author Martin
 */
public class ProdxpromControlador {
    public static Route getProdEnOferta = (Request request, Response response) -> {
      HashMap model = new HashMap();  
      ProdxpromDAO pDAO = new ProdxpromDAO();
      List<Producto> res = pDAO.selectProdConOfertas();
        
      model.put("productos", res);
      return ViewUtil.renderAjax(request, model, Path.Template.MOSTRAR_OFERTAS);    
    };
}
