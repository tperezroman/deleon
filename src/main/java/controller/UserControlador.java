/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.UserDAO;
import model.User;
import Principal.Menu;
import java.util.HashMap;
import java.util.List;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;
import util.Path;
import util.ViewUtil;

/**
 *
 * @author Ezequiel
 */
public class UserControlador {
    
    public static Route getLogin = (Request req, Response res) -> {
            HashMap model = new HashMap();
            
            if(req.queryParams("pass")!=null && req.queryParams("email")!=null){
                UserDAO uDAO = new UserDAO();
                List<User> user = uDAO.selectUsuario(req.queryParams("email"),req.queryParams("pass"));
                if(user.size() == 1){
                    //CREAR SEASION/COOKIE
                    User usuarioLogeado = user.get(0);
                    req.session(true);                     // Crear y retornar la sesion
                    req.session().attribute("id", usuarioLogeado.getId() );       // Seteamos atributo
                    req.session().attribute("email", usuarioLogeado.getEmail() ); // Seteamos atributo
                    res.redirect("/api/index/");
                }else{
                    model.put("request",req);
                    model.put("error", "La contraseña o el email es incorrecto.");
                }
                
            }
            else{
                model.put("email","");
            }
            

            return ViewUtil.renderLogin(req, model, Path.Template.LOGIN);
        }; 
    
    public static Route Logout = (Request req, Response res) -> {
            req.session().removeAttribute("email");
            res.redirect("/api/index/");
            return null;
        };
}
