/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;
import model.Pedido;
import model.DetallePedidoDAO;
import model.DetallePedido;
import model.PedidoDAO;
import model.Producto;

/**
 *
 * @author Martin
 */
public class DetallePedidoControlador {
    public static Route addProductoCarrito = (Request request, Response response) -> {
      HashMap model = new HashMap();  
      DetallePedidoDAO cDAO = new DetallePedidoDAO();
      int id_producto = Integer.parseInt(request.queryParams("id_producto"));
      
      String email = request.session().attribute("email");
      if(email==null){
          System.out.println("No estas logueado");
          //por que no redirecciona al login?
          response.redirect("/api/getLogin/");
          return ViewUtil.renderLogin(request, model, Path.Template.LOGIN);
      }
      else{
        System.out.println("Estas logueado");
        List<DetallePedido> aux = cDAO.selectxID(id_producto);
        if(aux.size()> 0){//porque si viene una lista vacia se rompe al hacer el get
            DetallePedido productoExistente = aux.get(0);
        
            System.out.println("DetalleCarrito en controlador antes de incrementar cantidad: " + productoExistente);
            int cantidad = productoExistente.getCantidad();
            if(cantidad > 0){//este esta de mas creo, porque si el prod ya existia en el carro
                //entonces ya tiene una cantidad mayor que cero
                cantidad = cantidad + 1;
                cDAO.update(productoExistente.getId_producto(),cantidad, productoExistente.getId_pedido());//podria pasarle el objeto entero y hacer el bind
            }
        }
        else{
            System.out.println("El producto no existia en el carro");
            PedidoDAO pDAO = new PedidoDAO();
            //obtengo el pedido que esta en proceso
            Pedido p = pDAO.getPedidoEnProceso();
            int id_pedido = p.getId_pedido();
            //Agreggo el producto con el id del pedido en proceso
            cDAO.agregarProducto(id_producto,id_pedido);
        }
        List<Producto> res = cDAO.selectProdDelCarro();
        model.put("productos", res);
      return ViewUtil.renderAjax(request, model, Path.Template.MOSTRAR_CARRITO);
      }
    };
   
    
    

    
    //para el popover del index
    public static Route getCarrito = (Request request, Response response) -> {
      HashMap model = new HashMap();
      DetallePedidoDAO cDAO = new DetallePedidoDAO();
      List<Producto> res = cDAO.selectProdDelCarro();
      model.put("productos", res);
      return ViewUtil.renderAjax(request, model, Path.Template.MOSTRAR_CARRITO);
    };
    
    //para la pagina entera de detalle del carro, con todos los productos y sus cantidades
    public static Route getDetalleCarrito = (Request request, Response response) -> {
      HashMap model = new HashMap();
      
      DetallePedidoDAO cDAO = new DetallePedidoDAO();
      List<Map<String,Object>> res = cDAO.selectComplejo();

      
      //solo para ver que imprime bien las cantidades de cada producto en detalle_carrito
//      res.forEach(m -> {
//            System.out.println("Cantidad : " + m.get("cantidad"));
//        });
      
      model.put("productos", res);

      return ViewUtil.render(request, model, Path.Template.MOSTRAR_DETALLE_CARRITO);
    };
}
