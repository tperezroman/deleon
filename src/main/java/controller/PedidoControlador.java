/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import model.DetallePedidoDAO;
import model.PedidoDAO;
import model.ProductoDAO;
import model.Producto;
import model.User;
import model.UserDAO;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.template.velocity.VelocityTemplateEngine;
import util.Path;
import util.ViewUtil;

/**
 *
 * @author martin
 */
public class PedidoControlador {
    public static Route getPedidos = (Request request, Response response) -> {
        HashMap model = new HashMap();

        PedidoDAO cDAO = new PedidoDAO();
        List<Map<String,Object>> res = cDAO.selectPedidos();
        List<Producto> p = new ArrayList();
        ProductoDAO pDAO = new ProductoDAO();

        Iterator<Map<String, Object>> i = res.iterator();

          while(i.hasNext()){
              int id_producto = (int) i.next().get("id_producto");
              p.add(pDAO.buscarProducto(id_producto));
          }

          model.put("productos", p);

          model.put("pedidos", res);

          return ViewUtil.render(request, model, Path.Template.LISTAR_COMPRAS);
    };
    
    
    //agregar pedido
    public static Route addPedido = (Request request, Response response) -> {
        HashMap model = new HashMap();

        PedidoDAO cDAO = new PedidoDAO();
        //primero agrego el pedido
        int total = Integer.parseInt(request.queryParams("total"));
        cDAO.actualizarPedido(total);
        //Hago un pedido en proceso para la proxima compra
        String email = request.session().attribute("email");
        UserDAO uDAO = new UserDAO();
        User u = uDAO.selectxEmail(email);
        int id_usuario = u.getId();
        //se hace el nuevo pedio con el id del usuario logueado
        cDAO.PedidoEnProceso(id_usuario);
        //despues recuperamos todos los pedidos
        List<Map<String,Object>> res = cDAO.selectPedidos();

  //      dDAO.vaciarCarro();

        model.put("productos", res);
        return ViewUtil.render(request, model, Path.Template.LISTAR_COMPRAS);
    };
    
    
    
    //mobile
    public static Route mgetPedidos = (Request request, Response response) -> {
      HashMap model = new HashMap();
      
      PedidoDAO cDAO = new PedidoDAO();
      List<Map<String,Object>> res = cDAO.selectPedidos();
      
      List<Producto> p = new ArrayList();
      ProductoDAO pDAO = new ProductoDAO();
      
      Iterator<Map<String, Object>> i = res.iterator();
        
        while(i.hasNext()){
            int id_producto = (int) i.next().get("id_producto");
            p.add(pDAO.buscarProducto(id_producto));
        }
        
        model.put("productos", p);
      
        model.put("pedidos", res);

        return ViewUtil.renderAjax(request, model, Path.Template.LISTAR_COMPRAS_MOBILE);
    };
    
    
}
