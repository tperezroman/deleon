/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.HashMap;
import model.CalificacionDAO;
import model.Producto;
import model.ProductoDAO;
import model.UserDAO;
import model.User;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;

/**
 *
 * @author Ezequiel
 */
public class CalificacionControlador {
    public static Route getCalificar = (Request request, Response response) -> {
      HashMap model = new HashMap();
      int id_producto = Integer.parseInt(request.queryParams("id_producto"));
      
      ProductoDAO pDAO = new ProductoDAO();
      Producto p = pDAO.buscarProducto(id_producto);
      model.put("producto", p);
      return ViewUtil.renderAjax(request, model, Path.Template.CALIFICAR);
    };
    
    public static Route saveCalificacion = (Request request, Response response) -> {
        HashMap model = new HashMap();
        String texto = request.queryParams("texto");
        int star = Integer.parseInt(request.queryParams("star"));
        int id_producto = Integer.parseInt(request.queryParams("id_producto"));
        String email = request.session().attribute("email");
        
        
        UserDAO uDAO = new UserDAO();
        User u = uDAO.selectxEmail(email);
        int id_usuario = u.getId();
        CalificacionDAO cDAO = new CalificacionDAO();
        cDAO.insertCalificacion(id_usuario, id_producto, star,texto);
        return ViewUtil.renderAjax(request, model, Path.Template.INDEX);
    };
}
