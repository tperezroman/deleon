/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import model.ProductoDAO;
import model.Producto;
import Principal.Menu;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import spark.Request;
import spark.Response;
import spark.Route;
import util.Path;
import util.ViewUtil;
/**
 *
 * @author Martin
 */
public class ProductoControlador {
    public static Route getIndex = (Request request, Response response) -> {
      HashMap model = new HashMap();
      ProductoDAO pDAO = new ProductoDAO();
      String email = request.session().attribute("email");
      ArrayList reg = new ArrayList();  
      if(email==null){
        reg.add("nulo");
        List<Producto> productos = pDAO.selectAll();
        model.put("productos", productos);
        model.put("registrado",reg);
        return ViewUtil.render(request, model, Path.Template.INDEX);
      }
      else{
        reg.add(email);
        List<Producto> productos = pDAO.selectAll();
        model.put("productos", productos);
        model.put("registrado",reg);
        return ViewUtil.render(request, model, Path.Template.INDEX);
      }
    };
    
    //este devuelve solo los productos, para despues insertarlo donde queramos con AJAX
    public static Route getTodos = (Request request, Response response) -> {
      HashMap model = new HashMap();
      ProductoDAO pDAO = new ProductoDAO();
      List<Producto> res = pDAO.selectAll();
      model.put("productos", res);
      return ViewUtil.renderAjax(request, model, Path.Template.INDEX);
    };
    
    //mobile
    public static Route mgetIndex = (Request request, Response response) -> {
      HashMap model = new HashMap();
      ProductoDAO pDAO = new ProductoDAO();
      String email = request.session().attribute("email");
      ArrayList reg = new ArrayList();  
      if(email==null){
        reg.add("nulo");
        List<Producto> productos = pDAO.selectAll();
        model.put("productos", productos);
        model.put("registrado",reg);
        return ViewUtil.renderAjax(request, model, Path.Template.INDEX_MOBILE);
      }
      else{
        reg.add(email);
        List<Producto> productos = pDAO.selectAll();
        model.put("productos", productos);
        model.put("registrado",reg);
        return ViewUtil.renderAjax(request, model, Path.Template.INDEX_MOBILE);
      }
    };
}
