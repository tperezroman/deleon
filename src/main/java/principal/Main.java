/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Principal;

import controller.CalificacionControlador;
import controller.UserControlador;
import controller.DetallePedidoControlador;
import controller.PedidoControlador;
import controller.ProductoControlador;
import controller.ProdxpromControlador;
import static spark.Spark.get;
import static spark.Spark.staticFiles;
import spark.Filter;
import static spark.Spark.after;
import static spark.Spark.post;
import util.Path;

/**
 *
 * @author Ezequiel
 */
public class Main {
    public static void main(String[] args) { 
        staticFiles.location("/public");
        
        after((Filter) (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");
            response.header("Access-Control-Allow-Methods", "POST");
        });
        
        get(Path.Web.LOGIN, UserControlador.getLogin );
        post(Path.Web.LOGIN,UserControlador.getLogin);
        get(Path.Web.LOGOUT, UserControlador.Logout);
        //prueba de caso de uso ver ofertas
        get(Path.Web.INDEX, ProductoControlador.getIndex );
        //prueba con tabla prodxprom
        get(Path.Web.GET_OFERTAS, ProdxpromControlador.getProdEnOferta);
        get(Path.Web.CALIFICAR, CalificacionControlador.getCalificar);
        get(Path.Web.GUARDARCALI, CalificacionControlador.saveCalificacion );
        //agregar al carrito
        post(Path.Web.AGREGARALCARRITO, DetallePedidoControlador.addProductoCarrito );
        get(Path.Web.MOSTRARCARRITO, DetallePedidoControlador.getCarrito);
        get(Path.Web.MOSTRARDETALLECARRITO, DetallePedidoControlador.getDetalleCarrito);
        //listar compras
        get(Path.Web.LISTARCOMPRAS, PedidoControlador.getPedidos);
        post(Path.Web.AGREGARPEDIDO, PedidoControlador.addPedido);
        //mobile        
        get(Path.Web.LISTARCOMPRAS_MOBILE,PedidoControlador.mgetPedidos);
        get(Path.Web.INDEX_MOBILE, ProductoControlador.mgetIndex );
    }
}
