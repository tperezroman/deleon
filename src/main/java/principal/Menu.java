package Principal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Ezequiel
 */
public class Menu {
    List<HashMap> menu = new ArrayList();
    public void init() {
            HashMap item = new HashMap();
            item.put("label", "Home");
            item.put("url", "/api/getIndex/");
            menu.add(item);
            item = new HashMap();
            item.put("label", "Login");
            item.put("url", "/api/getLogin/");
            menu.add(item);
    }
    
    public void initLog() {
            HashMap item = new HashMap();
            item.put("label", "Home");
            item.put("url", "/api/getIndex/");
            menu.add(item);
            item = new HashMap();
            item.put("label", "Logout");
            item.put("url", "/api/logout/");
            menu.add(item);
            item = new HashMap();
            item.put("label", "Equipos");
            item.put("url", "/api/getEquipos/");
            menu.add(item);
            item = new HashMap();
            item.put("label", "Jugadores");
            item.put("url", "/api/getJugadores/");
            menu.add(item);
    }
    
    
    public List<HashMap> getMenu() {
        //String email = req.session().attribute("email");
        init();
        return menu;
    }
    
    public List<HashMap> getMenuLog() {
        initLog();
        return menu;
    }
}