$(document).ready(function () {
//aca se colocan todos los metodos de JQuery para evitar que
//se ejecuten antes que termine de cargar a pagina
    
    
    //prueba ofertas con ajax
    $("#ofertas").click(function () {
        console.log("Entre a la funcion");
        $.ajax(
            {
                url: "http://localhost:4567/api/getOfertas/",
                type: "GET",
                success: function (data) {
                    $("#div1").html(data);
                    console.log(data);//para ver por console del navegador que contiene data
                }
            }
        );
    });
    //popover carrito de compras/////////////////////////////////////////////////////////////////////

    $('#carrito_popover').popover({
        container: "body",//esto va a evitar que el popover se desacomode cuando cambiamos el tamaño de la ventana
        trigger: "focus",
        container: "body",
        title: "<h2><a class='text-success' href='/api/getDetalleCarrito/'>Ver carrito</a></h2>",
        content: mostrarCarrito,
        html: true,
        placement: "top",
    });

    
    //para mostrar el carrito en el popover
    function mostrarCarrito() {
        var contenido_carrito = "";
        var reg = $("#carrito_popover").attr('reg');
        if(reg=="nulo"){
            location.href ="http://localhost:4567/api/getLogin/";
        }
        else{
            $.ajax(
                {
                    async: false,
                    type: "GET",//despues vamos a tener que apsarle un id asi que va a tener que ser post
                    url: "http://localhost:4567/api/getCarrito/",
                    success: function (data) {
                        console.log(data);
                        contenido_carrito = data;
                    }
                }
            );
        }
        return contenido_carrito;

    }
/////////////////////////////////////////////////////////////////////////////////////////////////////
    $(".carrito").click(function () {
        //el this hace referencia al boton que llamo al la funcion click 
        var id = $(this).attr('value');
        var reg = $(this).attr('reg');
        if(reg=="nulo"){
            location.href ="http://localhost:4567/api/getLogin/";
        }
         else {
            $.ajax({
                type: "POST",
                url: "http://localhost:4567/api/agregarAlCarrito/",
                data: {
                    "id_producto": id
                }
            });
        }  
    });

    $(".cerrar_carrito").click(function () {
        //el this hace referencia al boton que llamo al la funcion click 
        var total = $(this).attr('value');
        var reg = $(this).attr('reg');
        if(reg=="nulo"){
            location.href ="http://localhost:4567/api/getLogin/";
        }
         else {
            $.ajax({
                type: "POST",
                url: "http://localhost:4567/api/agregarPedido/",
                data: {
                    "total": total
                },
                success: function () {
                    location.href ="http://localhost:4567/api/getCompras/";
                }
            });
        }  
    });

    
    $("#btnSesion").click(function () {
        var reg = $(this).attr('value');
        $.ajax(
            {
                data: {
                    "email": reg
                },
                success: function (data) {
                    if(reg=="nulo"){
                        location.href ="http://localhost:4567/api/getLogin/";
                    }
                     else {
                        location.href="http://localhost:4567/api/index/#";
                    } 
                }
            }
        );
    });
    
    $("#btnLogout").click(function () {
        $.ajax({
                type: "GET",
                url: "http://localhost:4567/api/logout/",
                success: function () {
                    location.href ="http://localhost:4567/api/index/";
                }
        });
    });
    
    //Cuando cargo la pagina index, me fijo si esta logueado o no
    window.onload=function(){
        var reg = $('#btnSesion').attr('value');
        if(reg=="nulo"){
            $("#btnSesion a").html("Iniciar sesion");
            $("#btnLogout").hide();
        }
        else{
            $("#btnSesion a").html("Mi perfil");
        }
    };
    
    
    //cuando se dispare el evento show, se dispara esta funcion que oculta el popover luego de 2 segundos
    // $('#carrito_popover').on('show.bs.popover', function () {
    //     console.log("evento disparado");
    //     setTimeout(function () {
    //         $("#carrito_popover").popover('hide');
    //     }, 2000);
    // })

    $(".btnCalificar").click(
        function(){
            var id = $(this).val();
            $.ajax({
              type: 'get',
              url: '/api/calificar/',
              data: {id_producto: id },
                success: function (data) {
                    $(".modelo").html(data);
                }
            });
        });
        
    $(".btnListar").click(function () {
        var reg = $(this).attr('value');
        console.log("Entro");
        if(reg=="nulo"){
            location.href ="http://localhost:4567/api/getLogin/";
        }
        else{
            location.href ="http://localhost:4567/api/getCompras/";
        }
    });
});